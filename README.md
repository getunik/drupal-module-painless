# Overview
The Painless module provides an easier / less verbose way to implement reusable, configurable, code-centered panel panes. This reduction of boilerplate code and a more streamlined API make the whole process of authoring panel panes much more _painless_ ;)

Besides the usual panle pane stuff, the module allows / forces each Painless pane to define an individual theme hook with a template that provides easy customization of the generated markup through the theme engine.

# Getting Started
To add the module as a submodule, make sure to specify the submodule path as `painless` explicitly, because the module directory has to match the module name.
```bash
git submodule add repo_url painless
```

Check out the sample module in the `modules/example_painless` directory to get started. If you have panels set up properly, you can see how it works by:
* activate the example_painless module
* go to an existing or create a new panel page
* add a new pane and in the dialog, navigate to the _Painless_ category and pick the _Painless Example_ pane
* enter a name in the configuration dialog (or don't), confirm and save
* that's it - you should now be able to see the Painless example pane on your panel page

# Creating Custom Painless(es)

```php
function hook_register_painlesses()
{
	return array(
		'my_custom_painless' => array(
			'class' => '\Drupal\my_module\MyCustomPainless',
		),
	);
}
```

```php
namespace Drupal\my_module;


class MyCustomPainless extends \Drupal\painless\BasePainless
{
	public static function getMetadata()
	{
		return array(
			'pane_title' => 'My Custom Painless',
			'pane_description' => 'My Custom Painless',
		);
	}

	public function getConfigVariables($conf)
	{
		return array(
			'name' => array(
				'#type' => 'textfield',
				'#title' => t('Enter your name'),
				'#description' => t('The name will be displayed as part of this Painless pane'),
				'#default_value' => $conf['name'],
			),
		);
	}

	public function preprocess(&$vars, $conf, $args, $contexts)
	{
		$vars['text_snippet'] = 'Hello ' . $conf['name'] . '. How you doin\'?';
	}
}

```
