<?php

$plugin = array(
	'title' => t('Painless'), // Title to show up on the pane screen.
	'description' => t('Painless content type'), // Description to show up on the pane screen.
	'category' => t('Painless'), // A category to put this under.
	'content types' => 'painless_get_painless_content_types',
	// 'defaults' => array( // Array of defaults for the settings form.
	// 	'label_var' => 'floating_footer_label',
	// ),
	'all contexts' => TRUE, // This is NEEDED to be able to use substitution strings in your pane.
);

function painless_get_painless_content_types()
{
	$types = array();

	foreach (get_all_painless_types() as $name => $conf)
	{
		$types[$name] = array(
			'title' => $conf['meta']['pane_title'],
			'description' => $conf['meta']['pane_description'],
			//'icon' => ctools_content_admin_icon($plugin),
			'category' => t('Painless'),
			//'required context' => $plugin['required context'],
		);
	}

	return $types;
}

/**
 * {module}_{plugin-name}_content_type_edit_form
 */
function painless_painless_content_type_edit_form($form, &$form_state)
{
	$inst = painless_get_instance($form_state['subtype_name']);
	$inst->editForm($form, $form_state);

	return $form;
}

/**
 * {module}_{plugin-name}_content_type_edit_form_submit
 */
function painless_painless_content_type_edit_form_submit(&$form, &$form_state)
{
	$inst = painless_get_instance($form_state['subtype_name']);

	foreach ($inst->getConfigVariables($form_state['conf']) as $key => $config)
	{
		if (isset($form_state['values'][$key]))
		{
			$form_state['conf'][$key] = $form_state['values'][$key];
		}
	}
}

/**
 * {module}_{plugin-name}_content_type_render
 */
function painless_painless_content_type_render($subtype, $conf, $args, $contexts)
{
	$inst = painless_get_instance($subtype);

	$block = new stdClass();
	$block->title = ''; // This will be overridden by the user within the panel options.
	$block->content = $inst->render($conf, $args, $contexts);

	return $block;
}

/**
 * {module}_{plugin-name}_content_type_admin_info
 */
function painless_painless_content_type_admin_info($subtype, $conf, $contexts)
{
	$inst = painless_get_instance($subtype);

	if (!empty($conf))
	{
		$block = new stdClass;
		$block->title = $inst->getMeta('pane_title');
		$block->content = $inst->getAdminInfo($conf, $contexts);
		return $block;
	}
}
