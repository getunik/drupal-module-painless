<?php ?>
<h3>Painless Example</h3>
<div>
	<p>This is the default Painless template provided by the module</p>
	<p>It can be overwritten in the theme simply by creating a template with the same name</p>
</div>
<blockquote><?php print $text_snippet; ?></blockquote>
