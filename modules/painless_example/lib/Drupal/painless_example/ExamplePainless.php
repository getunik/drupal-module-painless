<?php

namespace Drupal\painless_example;


class ExamplePainless extends \Drupal\painless\BasePainless
{
	public static function getMetadata()
	{
		return array(
			'pane_title' => 'Painless Example',
			'pane_description' => 'Simple Painless pane implementation',
		);
	}

	public function getConfigVariables($conf)
	{
		return array(
			'name' => array(
				'#type' => 'textfield',
				'#title' => t('Enter your name'),
				'#description' => t('The name will be displayed as part of this Painless pane'),
				'#default_value' => $conf['name'],
			),
		);
	}

	public function preprocess(&$vars, $conf, $args, $contexts)
	{
		$vars['text_snippet'] = 'Hello ' . $conf['name'] . '. How you doin\'?';
	}
}
