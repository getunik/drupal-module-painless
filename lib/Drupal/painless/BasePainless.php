<?php

namespace Drupal\painless;


abstract class BasePainless
{
	protected $typeDef;

	public function __construct($typeDef)
	{
		$this->typeDef = $typeDef;
	}

	public function getMeta($name = NULL)
	{
		return ($name === NULL ? $this->typeDef['meta'] : $this->typeDef['meta'][$name]);
	}

	public function editForm(&$form, &$form_state)
	{
		$conf = $form_state['conf'];

		foreach ($this->getConfigVariables($conf) as $name => $field)
		{
			$field['#default_value'] = $conf[$name];
			$form[$name] = $field;
		}
	}

	public function getAdminInfo($conf, $contexts)
	{
		 return 'Configuration: ' . json_encode($conf, JSON_PRETTY_PRINT);
	}

	public function getConfigVariables($conf)
	{
	}

	public function render($conf, $args, $contexts)
	{
		// Allow ctools keyword substitution for configuration values
		foreach($conf as &$configValue) {
			$configValue = ctools_context_keyword_substitute($configValue, array(), $contexts);
		}

		$content = array(
			'#theme' => $this->getMeta('element'),
			'#painless' => $this,
			'#conf' => $conf,
			'#args' => $args,
			'#contexts' => $contexts,
		);

		$result = $this->processRenderArray($content, $conf, $args, $contexts);
		return render($result);
	}

	public function processRenderArray(&$renderArray)
	{
		return $renderArray;
	}

	public function preprocess(&$vars, $conf, $args, $contexts)
	{
	}
}
